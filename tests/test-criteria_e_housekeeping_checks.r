
################################################################
# name:criteria_e_housekeeping_checks test
#### Set the project home ####
projectdir <- "~/projects/IUCNcriteriaEmountainashforests"
# this will be used as the base directory to add subdirectories like data,
# code, graphs and reports etc
setwd(projectdir)
#### Set the directory to source the scripts from ####
codedir <- "~/projects/IUCNcriteriaEmountainashforests/inst/doc"
# In this example we use a standard R package structure, and the
# inst/doc location is recommended to store arbitrary manuals and
# helpful documents that provide additional supporting information to
# the package vignettes and help files
# NB when this package is built and deployed these R script files are moved to the doc directory 
# file.path(system.file(package="IUCNcriteriaEmountainashforests"),"doc")

##### Set the directory to source the data from ####
datadir <- "~/projects/IUCNcriteriaEmountainashforests/Data"
# NB these data are not supplied with the R package
infile_stag <- "stag_data_all_years.csv"
infile_ash <- "ash_fire_matrix.csv"
   
#### Load ####
# Housekeeping_Checks
criteria_e_housekeeping_checks(
  projectdir = getwd(),
  codedir = "./inst/doc",
  datadir = "./Data",
  infile_stag = "stag_data_all_years.csv",
  infile_ash = "ash_fire_matrix.csv"
)
