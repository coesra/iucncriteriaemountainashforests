
################################################################
# name:sim.discount

sim.discount<-function(discount.fac,length=.05)
{
res<-rep(0,length(discount.fac))
for(i in 1:length(discount.fac)) {
lower<-ifelse(discount.fac[i]<2*length,discount.fac[i],discount.fac[i]-length)
upper<-ifelse(1-discount.fac[i]<2*length,discount.fac[i],discount.fac[i]+length)
res[i]<-runif(1,lower,upper)
}
return(res)
}
