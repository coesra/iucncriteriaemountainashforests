
################################################################
# name:simulation.function.other.scenarios
#simulation function - used to generate the projection probability distributions used in part E.  This is achieved by allowing variability in
# the various input parameters by using a coefficient of variation of 17.3% or something deemed more appropriate for certain parameters.

simulation.function.other.scenarios<-function(Nsim=10,cv=0.173,salvage.log.base=3500,trp1983.base=151,trp1939.base=586,discount.length=.05,
avail.logging.area=c(0,21741,7360),area.unburned=c(1713,51619,11185),burnt.area.unlogged.2009=c(3100,35350,53400-3100-35350),
fire.mult.1939=0.9575470,fire.mult.1983=0.1094486,fire.mult.2009=0.3385079,total.2011.area=sum(c(area.unburned,burnt.area.unlogged.2009))  )
{
# TODO check that setting the seed here leads to same sims across the board?
set.seed(123)

# Nsim = the number of simulations to compute
# cv = the coefficient of variation used
# salavage.log.base = the amount of salvage logging allowed
# trp1983.base = the amount of 1983 regrowth logging allowed
# trp1939.base = the amount of 1939 regrowth logging allowed
# discount.length = the amount of variation in the discount factors.
# avail.logging.area = the amount of logging area by age class ( old growth, 1939 regrowth, 1983 regrowth)
# area.unburned = the area unburned in 2009 by age class
# burned.area.unlogged = the area that was burned in 2009 but unlogged
# fire.mult.1939 = the extent of the 1939 fire
# fire.mult.1983 = the extent of the 1983 fire
# fire.mult.2009 = the extent of the 2009 fire
# total.2011.area = the total extent of the ecosytem in 2011.

#assuming the discount factors are not subject to variation as this is hard to model!!
# not true, they are difficult to model properly, apply the cv to the proportion as per the other variables.
#assuming that the initial areas are measured without error
#allowing variability in the following variables
#number of stags per hectare in 2011,
#the amount of take in logging for 1939 regrowth and the 1983 regrowth
#the area of the fire in all scenarios
# amount of salvage logging
# we are using 3500 for the 2009 fire, 0.1094486/0.3385079 for the 1983 fire and 0.9575470/0.3385079 fore the 1939 fire.
# assuming that the proportion of fire varies


sim.res<-array(0,dim=c(Nsim,3,13,4)) # 3 logging scenarios: none, 1983+1939 logging, 1983 only, 13 fire scenarios: 1 no fire, + 12 other regimes

# the number of stems per hectare, using the variances compute from the stem data.

s.no.stags.2011.nf<-rmvnorm(Nsim,mean=ash.no.stags.2011.nf,sigma=diag(ash.var.no.stags.2011.nf))
s.no.stags.2011.af<-rmvnorm(Nsim,mean=ash.no.stags.2011.af,sigma=diag(ash.var.no.stags.2011.af))
#s.fire.mult.1939<-runif(Nsim,min=.77,max=.97)
#s.fire.mult.1983<-runif(Nsim,min=.05,max=.25)
#s.fire.mult.2009<-runif(Nsim,min=.25,max=.45)
cv.fire.area<-0.05
#s.fire.mult.1939<-runif(Nsim,min=.77,max=.97) #need to change this later  # need to consider using the beta for these so the proportions can't go outside 0-1
#s.fire.mult.1983<-rnorm(Nsim,mean=fire.mult.1983,sd=fire.mult.1983*cv.fire.area)
#s.fire.mult.2009<-rnorm(Nsim,mean=fire.mult.2009,sd=fire.mult.2009*cv.fire.area)
#going to use beta distributions here:
# assuming 0.11 for 1939  # assuming 11% error in the extent for the 1939 fire and 5% for the 1983 and 2009 fire.
# assuming 0.05 for 1983 and 2009
est.1939<-fire.mult.1939-0.128

#use these to estimate a beta distribution with parameters a, b given the mean and variance..
ab.1939<-ab.function(est.1939,.11^2)  # assuming the errors +/- the estimate, of course this isn't strictly true.
ab.1983<-ab.function(fire.mult.1983,.05^2)
ab.2009<-ab.function(fire.mult.2009,.05^2)

s.fire.mult.1939<-rbeta(Nsim,ab.1939$a,ab.1939$b)
s.fire.mult.1983<-rbeta(Nsim,ab.1983$a,ab.1983$b)
s.fire.mult.2009<-rbeta(Nsim,ab.2009$a,ab.2009$b)

#allow the salavage log area to vary according to the cv.
s.salvage.log<-rnorm(Nsim,mean=salvage.log.base,sd=salvage.log.base*cv)
s.trp.1983<-rnorm(Nsim,mean=trp1983.base,sd=trp1983.base*cv)
s.trp.1939<-rnorm(Nsim,mean=trp1939.base,sd=trp1939.base*cv)
s.salvage.log.2009<-s.salvage.log
s.salvage.log.1983<-s.salvage.log*s.fire.mult.1983/s.fire.mult.2009
s.salvage.log.1939<-s.salvage.log*s.fire.mult.1939/s.fire.mult.2009
for ( i in 1:Nsim) {


# simulate the discount factors, by simulating from a uniform distribution with a given length
# we make appropriate modifications depending on where the discount factor is located (see sim.discount).

discount.factors.f0000<-list(
y2025=sim.discount(y2025.discount.fac.ash.f0000,length=discount.length),
y2039=sim.discount(y2039.discount.fac.ash.f0000,length=discount.length),
y2053=sim.discount(y2053.discount.fac.ash.f0000,length=discount.length),
y2067=sim.discount(y2067.discount.fac.ash.f0000,length=discount.length))

discount.factors.f1000<-list(
y2025.n=discount.factors.f0000$y2025,
y2039.n=discount.factors.f0000$y2039,
y2053.n=discount.factors.f0000$y2053,
y2067.n=discount.factors.f0000$y2067,
y2025.f=sim.discount(y2025.discount.fac.ash.f1000,length=discount.length),
y2039.f=sim.discount(y2039.discount.fac.ash.f1000,length=discount.length),
y2053.f=sim.discount(y2053.discount.fac.ash.f1000,length=discount.length),
y2067.f=sim.discount(y2067.discount.fac.ash.f1000,length=discount.length))

discount.factors.f0100<-list(
y2025.n=discount.factors.f0000$y2025,
y2039.n=discount.factors.f0000$y2039,
y2053.n=discount.factors.f0000$y2053,
y2067.n=discount.factors.f0000$y2067,
y2025.f=sim.discount(y2025.discount.fac.ash.f0100,length=discount.length),
y2039.f=sim.discount(y2039.discount.fac.ash.f0100,length=discount.length),
y2053.f=sim.discount(y2053.discount.fac.ash.f0100,length=discount.length),
y2067.f=sim.discount(y2067.discount.fac.ash.f0100,length=discount.length))

discount.factors.f0010<-list(
y2025.n=discount.factors.f0000$y2025,
y2039.n=discount.factors.f0000$y2039,
y2053.n=discount.factors.f0000$y2053,
y2067.n=discount.factors.f0000$y2067,
y2025.f=sim.discount(y2025.discount.fac.ash.f0010,length=discount.length),
y2039.f=sim.discount(y2039.discount.fac.ash.f0010,length=discount.length),
y2053.f=sim.discount(y2053.discount.fac.ash.f0010,length=discount.length),
y2067.f=sim.discount(y2067.discount.fac.ash.f0010,length=discount.length))


discount.factors.f0001<-list(
y2025.n=discount.factors.f0000$y2025,
y2039.n=discount.factors.f0000$y2039,
y2053.n=discount.factors.f0000$y2053,
y2067.n=discount.factors.f0000$y2067,
y2025.f=sim.discount(y2025.discount.fac.ash.f0001,length=discount.length),
y2039.f=sim.discount(y2039.discount.fac.ash.f0001,length=discount.length),
y2053.f=sim.discount(y2053.discount.fac.ash.f0001,length=discount.length),
y2067.f=sim.discount(y2067.discount.fac.ash.f0001,length=discount.length))


# Generate the 3 x 13 scenarios (x 4 fire intervals)

#no fire: 3 scenarios: no logging, current logging practice 1939 and 1983 regrowth, 1983 regrowth only.
# no harvesting:
sim.res[i,1,1,]<-no.har.no.fire(s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.factors.f0000,
area.2011.unharvested,burnt.area.unlogged.2009,total.2011.area)


fire.no.logging.f1000.sm.1983<-fire.fun.no.logging(1,s.fire.mult.1983[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)
fire.no.logging.f1000.md.2009<-fire.fun.no.logging(1,s.fire.mult.2009[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)
fire.no.logging.f1000.lg.1939<-fire.fun.no.logging(1,s.fire.mult.1939[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)

fire.no.logging.f0100.sm.1983<-fire.fun.no.logging(2,s.fire.mult.1983[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)
fire.no.logging.f0100.md.2009<-fire.fun.no.logging(2,s.fire.mult.2009[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)
fire.no.logging.f0100.lg.1939<-fire.fun.no.logging(2,s.fire.mult.1939[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)

fire.no.logging.f0010.sm.1983<-fire.fun.no.logging(3,s.fire.mult.1983[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)
fire.no.logging.f0010.md.2009<-fire.fun.no.logging(3,s.fire.mult.2009[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)
fire.no.logging.f0010.lg.1939<-fire.fun.no.logging(3,s.fire.mult.1939[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)

fire.no.logging.f0001.sm.1983<-fire.fun.no.logging(4,s.fire.mult.1983[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)
fire.no.logging.f0001.md.2009<-fire.fun.no.logging(4,s.fire.mult.2009[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)
fire.no.logging.f0001.lg.1939<-fire.fun.no.logging(4,s.fire.mult.1939[i],burnt.area.unlogged=burnt.area.unlogged.2009,area.unburned=area.unburned)


sim.res[i,1,2,]<-stems.per.hectare.fun(fire.no.logging.f1000.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f1000,total.area=total.2011.area)
sim.res[i,1,3,]<-stems.per.hectare.fun(fire.no.logging.f0100.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0100,total.area=total.2011.area)
sim.res[i,1,4,]<-stems.per.hectare.fun(fire.no.logging.f0010.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0010,total.area=total.2011.area)
sim.res[i,1,5,]<-stems.per.hectare.fun(fire.no.logging.f0001.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0001,total.area=total.2011.area)

sim.res[i,1,6,]<-stems.per.hectare.fun(fire.no.logging.f1000.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f1000,total.area=total.2011.area)
sim.res[i,1,7,]<-stems.per.hectare.fun(fire.no.logging.f0100.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0100,total.area=total.2011.area)
sim.res[i,1,8,]<-stems.per.hectare.fun(fire.no.logging.f0010.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0010,total.area=total.2011.area)
sim.res[i,1,9,]<-stems.per.hectare.fun(fire.no.logging.f0001.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0001,total.area=total.2011.area)

sim.res[i,1,10,]<-stems.per.hectare.fun(fire.no.logging.f1000.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f1000,total.area=total.2011.area)
sim.res[i,1,11,]<-stems.per.hectare.fun(fire.no.logging.f0100.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0100,total.area=total.2011.area)
sim.res[i,1,12,]<-stems.per.hectare.fun(fire.no.logging.f0010.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0010,total.area=total.2011.area)
sim.res[i,1,13,]<-stems.per.hectare.fun(fire.no.logging.f0001.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0001,total.area=total.2011.area)


#usual practice
logging.area.projections.f0000<- rep(c(0,s.trp.1939[i],s.trp.1983[i]),4)*rep(c(14,28,42,56),each=3)
logging.area.projections.f0000<-ifelse(logging.area.projections.f0000<rep(avail.logging.area,4),logging.area.projections.f0000,rep(avail.logging.area,4))
unlogged.unburnt.area.projections.f0000<-rep(area.unburned,4)-logging.area.projections.f0000

sim.res[i,2,1,]<-har.no.fire(s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.factors.f0000,
unlogged.unburnt.area.projections.f0000,burnt.area.unlogged.2009,total.2011.area)


fire.logging.f1000.sm.1983<-logging.fun.with.fire(1,s.fire.mult.1983[i],slav.after.fire=s.salvage.log.1983[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)
fire.logging.f1000.md.2009<-logging.fun.with.fire(1,s.fire.mult.2009[i],slav.after.fire=s.salvage.log.2009[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)
fire.logging.f1000.lg.1939<-logging.fun.with.fire(1,s.fire.mult.1939[i],slav.after.fire=s.salvage.log.1939[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)

fire.logging.f0100.sm.1983<-logging.fun.with.fire(2,s.fire.mult.1983[i],slav.after.fire=s.salvage.log.1983[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)
fire.logging.f0100.md.2009<-logging.fun.with.fire(2,s.fire.mult.2009[i],slav.after.fire=s.salvage.log.2009[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)
fire.logging.f0100.lg.1939<-logging.fun.with.fire(2,s.fire.mult.1939[i],slav.after.fire=s.salvage.log.1939[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)

fire.logging.f0010.sm.1983<-logging.fun.with.fire(3,s.fire.mult.1983[i],slav.after.fire=s.salvage.log.1983[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)
fire.logging.f0010.md.2009<-logging.fun.with.fire(3,s.fire.mult.2009[i],slav.after.fire=s.salvage.log.2009[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)
fire.logging.f0010.lg.1939<-logging.fun.with.fire(3,s.fire.mult.1939[i],slav.after.fire=s.salvage.log.1939[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)

fire.logging.f0001.sm.1983<-logging.fun.with.fire(4,s.fire.mult.1983[i],slav.after.fire=s.salvage.log.1983[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)
fire.logging.f0001.md.2009<-logging.fun.with.fire(4,s.fire.mult.2009[i],slav.after.fire=s.salvage.log.2009[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)
fire.logging.f0001.lg.1939<-logging.fun.with.fire(4,s.fire.mult.1939[i],slav.after.fire=s.salvage.log.1939[i],trp1939=s.trp.1939[i],
trp1983=s.trp.1983[i],avail.logging.area=avail.logging.area,area.unburned=area.unburned)

sim.res[i,2,2,]<-stems.per.hectare.fun(fire.logging.f1000.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f1000,total.area=total.2011.area)
sim.res[i,2,3,]<-stems.per.hectare.fun(fire.logging.f0100.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0100,total.area=total.2011.area)
sim.res[i,2,4,]<-stems.per.hectare.fun(fire.logging.f0010.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0010,total.area=total.2011.area)
sim.res[i,2,5,]<-stems.per.hectare.fun(fire.logging.f0001.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0001,total.area=total.2011.area)

sim.res[i,2,6,]<-stems.per.hectare.fun(fire.logging.f1000.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f1000,total.area=total.2011.area)
sim.res[i,2,7,]<-stems.per.hectare.fun(fire.logging.f0100.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0100,total.area=total.2011.area)
sim.res[i,2,8,]<-stems.per.hectare.fun(fire.logging.f0010.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0010,total.area=total.2011.area)
sim.res[i,2,9,]<-stems.per.hectare.fun(fire.logging.f0001.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0001,total.area=total.2011.area)

sim.res[i,2,10,]<-stems.per.hectare.fun(fire.logging.f1000.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f1000,total.area=total.2011.area)
sim.res[i,2,11,]<-stems.per.hectare.fun(fire.logging.f0100.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0100,total.area=total.2011.area)
sim.res[i,2,12,]<-stems.per.hectare.fun(fire.logging.f0010.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0010,total.area=total.2011.area)
sim.res[i,2,13,]<-stems.per.hectare.fun(fire.logging.f0001.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0001,total.area=total.2011.area)


# logging 1983 regrowth only

trp.harvest.1983.only<-c(0,0,s.trp.1983[i])

avail.harvest.1983.only<-avail.logging.area
avail.harvest.1983.only[2]<-0

logging.1983.only.area.projections.f0000<- rep(trp.harvest.1983.only,4)*rep(c(14,28,42,56),each=3)
logging.1983.only.area.projections.f0000<-ifelse(logging.1983.only.area.projections.f0000<rep(avail.harvest.1983.only,4),logging.1983.only.area.projections.f0000,
rep(avail.harvest.1983.only,4))
unlogged.1983.only.unburnt.area.projections.f0000<-rep(area.2011.unharvested,4)-logging.1983.only.area.projections.f0000

sim.res[i,3,1,]<-har.no.fire(no.stags.nf=s.no.stags.2011.nf[i,],no.stags.af=s.no.stags.2011.af[i,],discount.fac=discount.factors.f0000,
area.unharv.proj=unlogged.1983.only.unburnt.area.projections.f0000,burnt.unlogged=burnt.area.unlogged.2009,total.area=total.2011.area)


fire.logging.1983.only.f1000.sm.1983<-logging.fun.with.fire(1,s.fire.mult.1983[i],slav.after.fire=s.salvage.log.1983[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)
fire.logging.1983.only.f1000.md.2009<-logging.fun.with.fire(1,s.fire.mult.2009[i],slav.after.fire=s.salvage.log.2009[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)
fire.logging.1983.only.f1000.lg.1939<-logging.fun.with.fire(1,s.fire.mult.1939[i],slav.after.fire=s.salvage.log.1939[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)


fire.logging.1983.only.f0100.sm.1983<-logging.fun.with.fire(2,s.fire.mult.1983[i],slav.after.fire=s.salvage.log.1983[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)
fire.logging.1983.only.f0100.md.2009<-logging.fun.with.fire(2,s.fire.mult.2009[i],slav.after.fire=s.salvage.log.2009[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)
fire.logging.1983.only.f0100.lg.1939<-logging.fun.with.fire(2,s.fire.mult.1939[i],slav.after.fire=s.salvage.log.1939[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)


fire.logging.1983.only.f0010.sm.1983<-logging.fun.with.fire(3,s.fire.mult.1983[i],slav.after.fire=s.salvage.log.1983[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)
fire.logging.1983.only.f0010.md.2009<-logging.fun.with.fire(3,s.fire.mult.2009[i],slav.after.fire=s.salvage.log.2009[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)
fire.logging.1983.only.f0010.lg.1939<-logging.fun.with.fire(3,s.fire.mult.1939[i],slav.after.fire=s.salvage.log.1939[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)


fire.logging.1983.only.f0001.sm.1983<-logging.fun.with.fire(4,s.fire.mult.1983[i],slav.after.fire=s.salvage.log.1983[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)
fire.logging.1983.only.f0001.md.2009<-logging.fun.with.fire(4,s.fire.mult.2009[i],slav.after.fire=s.salvage.log.2009[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)
fire.logging.1983.only.f0001.lg.1939<-logging.fun.with.fire(4,s.fire.mult.1939[i],slav.after.fire=s.salvage.log.1939[i],trp1939=0,trp1983=s.trp.1983[i],
burnt.area.unlogged=burnt.area.unlogged.2009,avail.logging.area=avail.harvest.1983.only,area.unburned=area.unburned)

sim.res[i,3,2,]<-stems.per.hectare.fun(fire.logging.1983.only.f1000.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f1000,total.area=total.2011.area)
sim.res[i,3,3,]<-stems.per.hectare.fun(fire.logging.1983.only.f0100.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0100,total.area=total.2011.area)
sim.res[i,3,4,]<-stems.per.hectare.fun(fire.logging.1983.only.f0010.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0010,total.area=total.2011.area)
sim.res[i,3,5,]<-stems.per.hectare.fun(fire.logging.1983.only.f0001.sm.1983,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0001,total.area=total.2011.area)

sim.res[i,3,6,]<-stems.per.hectare.fun(fire.logging.1983.only.f1000.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f1000,total.area=total.2011.area)
sim.res[i,3,7,]<-stems.per.hectare.fun(fire.logging.1983.only.f0100.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0100,total.area=total.2011.area)
sim.res[i,3,8,]<-stems.per.hectare.fun(fire.logging.1983.only.f0010.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0010,total.area=total.2011.area)
sim.res[i,3,9,]<-stems.per.hectare.fun(fire.logging.1983.only.f0001.md.2009,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0001,total.area=total.2011.area)

sim.res[i,3,10,]<-stems.per.hectare.fun(fire.logging.1983.only.f1000.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f1000,total.area=total.2011.area)
sim.res[i,3,11,]<-stems.per.hectare.fun(fire.logging.1983.only.f0100.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0100,total.area=total.2011.area)
sim.res[i,3,12,]<-stems.per.hectare.fun(fire.logging.1983.only.f0010.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0010,total.area=total.2011.area)
sim.res[i,3,13,]<-stems.per.hectare.fun(fire.logging.1983.only.f0001.lg.1939,s.no.stags.2011.nf[i,],s.no.stags.2011.af[i,],discount.fac=discount.factors.f0001,total.area=total.2011.area)



}

return(sim.res)
}
