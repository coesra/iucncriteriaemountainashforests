
################################################################
# name:ab.function
# a function to solve for the a,b parameter of a beta distribution given the mean and variance, (m,v).
# compute the parameters of a beta distribution, given the mean(m) and variance(v)
ab.function<-function(m,v) { 
list(a=-m*(m^2-m+v)/v, b=(m^2-m+v)*(m-1)/v) 
}
