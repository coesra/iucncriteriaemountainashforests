#' @name iucn_housekeeping_checks
#' @title checks on depends iucn e
#'
#' @param projectdir proj dir
#' @param codedir code dir
#' @param datadir data dir
#' @param infile_stag infile
#' @param infile_ash infile2
#' @return print warning
iucn_housekeeping_checks <- function(projectdir, codedir, datadir, infile_stag, infile_ash){
  # this is on CRAN so can be open sourced
  if(!require(mvtnorm)) install.packages("mvtnorm"); library(mvtnorm)
  txt <- ""
  # this is closed source code so need access
  if(!require(IUCNcriteriaEmountainashforests)){
    txt  <- paste(txt, "Please use the devtools package to run the command install_bitbucket('CoESRA/IUCNcriteriaEmountainashforests', ref = 'devel')", sep = "\n")
  }
  if(!file.exists(codedir)) {
    txt  <- paste(txt,"codedir does not exist. Please put the code in the codedir", sep = "\n")
  }

  # this data is mediated access
  if(!file.exists(file.path(datadir, infile_stag))){
    txt  <- paste(txt, "stag data does not exist. Please contact <david.lindenmayer@anu.edu.au> put the data in the datadir", sep = "\n")
  }
  if(!file.exists(file.path(datadir, infile_ash ))){
    txt  <- paste(txt, "ash data does not exist. Please put the data in the datadir", sep  = "\n")
  }

  if(
    nchar(txt) == 0
    ){
    print("Your all set")
  } else {
    print(cat(txt))
  }
}
